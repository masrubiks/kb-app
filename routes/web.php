<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{BkbController, BklController, BkrController, HomeController, DashboardController, HelperController, PikrController, UppksController};

Route::get('/', HomeController::class)->name('home');
Route::get('dashboard', DashboardController::class)->name('dashboard');

Route::prefix('tribina')->group(function () {
    Route::get('bkb', [BkbController::class, 'index'])->name('bkb');
    Route::get('bkb/create', [BkbController::class, 'create'])->name('bkb.create');
    Route::post('bkb/create', [BkbController::class, 'store']);

    Route::get('bkr', [BkrController::class, 'index'])->name('bkr');
    Route::get('bkr/create', [BkrController::class, 'create'])->name('bkr.create');

    Route::get('bkl', [BklController::class, 'index'])->name('bkl');
    Route::get('bkl/create', [BklController::class, 'create'])->name('bkl.create');

    Route::get('uppks', [UppksController::class, 'index'])->name('uppks');
    Route::get('uppks/create', [UppksController::class, 'create'])->name('uppks.create');

    Route::get('pikr', [PikrController::class, 'index'])->name('pikr');
    Route::get('pikr/create', [PikrController::class, 'create'])->name('pikr.create');
});

Route::prefix('helper')->group(function () {
    Route::post('getkab', [HelperController::class, 'get_kab'])->name('helper.getkab');
    Route::post('getkec', [HelperController::class, 'get_kec'])->name('helper.getkec');
    Route::post('getkel', [HelperController::class, 'get_kel'])->name('helper.getkel');
    Route::post('getnik', [HelperController::class, 'get_nik'])->name('helper.getnik');
});
