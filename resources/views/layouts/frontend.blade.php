@extends('layouts.app')
@section('body')

<body id="kt_body" class="header-fixed header-mobile-fixed header-bottom-enabled aside-enabled aside-static">
    <!--begin::Header Mobile-->
    <div id="kt_header_mobile" class="header-mobile bg-primary header-mobile-fixed">
        <!--begin::Logo-->
        <a href="/metronic/demo7/index.html">
            <img alt="Logo" src="{{ asset('media/logos/logo-letter-9.png') }}" class="max-h-30px">
        </a>
        <!--end::Logo-->
        <!--begin::Toolbar-->
        <div class="d-flex align-items-center">
            <button class="btn p-0 burger-icon burger-icon-left" id="kt_aside_mobile_toggle">
                <span></span>
            </button>
            <button class="btn p-0 burger-icon burger-icon-left ml-4" id="kt_header_mobile_toggle">
                <span></span>
            </button>
            <button class="btn p-0 ml-2" id="kt_header_mobile_topbar_toggle">
                <span class="svg-icon svg-icon-xl">
                    <!--begin::Svg Icon | path:/metronic/theme/html/demo7/dist/assets/media/svg/icons/General/User.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                        height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                            <path
                                d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z"
                                fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                            <path
                                d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z"
                                fill="#000000" fill-rule="nonzero"></path>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
            </button>
        </div>
        <!--end::Toolbar-->
    </div>
    <!--end::Header Mobile-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="d-flex flex-row flex-column-fluid page">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                <!--begin::Header-->
                <div id="kt_header" class="header flex-column header-fixed">
                    <!--begin::Top-->
                    <div class="header-top">
                        <!--begin::Container-->
                        <div class="container-fluid">
                            <!--begin::Left-->
                            <div class="d-none d-lg-flex align-items-center mr-3">
                                <!--begin::Logo-->
                                <a href="{{ route('home') }}" class="mr-20">
                                    <img alt="Logo" src="{{ asset('media/logo-bkkbn.png') }}" class="max-h-35px">
                                </a>
                                <!--end::Logo-->
                                <!--begin::Tab Navs(for desktop mode)-->
                                <ul class="header-tabs nav align-self-end font-size-lg" role="tablist">
                                    <!--begin::Item-->
                                    <li class="nav-item">
                                        <a href="#" class="nav-link py-4 px-6 active" data-toggle="tab"
                                            data-target="#kt_header_tab_1" role="tab">Kampung KB</a>
                                    </li>
                                    <!--end::Item-->
                                </ul>
                                <!--begin::Tab Navs-->
                            </div>
                            <!--end::Left-->
                            <!--begin::Topbar-->
                            <div class="topbar bg-primary">
                                <!--begin::User-->
                                <div class="topbar-item">
                                    <div class="btn btn-icon btn-hover-transparent-white w-sm-auto d-flex align-items-center btn-lg px-2"
                                        id="kt_quick_user_toggle">
                                        <div class="d-flex flex-column text-right pr-sm-3">
                                            <span
                                                class="text-white opacity-50 font-weight-bold font-size-sm d-none d-sm-inline">MasBen</span>
                                            <span
                                                class="text-white font-weight-bolder font-size-sm d-none d-sm-inline">Administrator</span>
                                        </div>
                                    </div>
                                </div>
                                <!--end::User-->
                            </div>
                            <!--end::Topbar-->
                        </div>
                        <!--end::Container-->
                    </div>
                    <!--end::Top-->
                    <!--begin::Bottom-->
                    <div class="header-bottom">
                        <!--begin::Container-->
                        <div class="container-fluid">
                            <!--begin::Header Menu Wrapper-->
                            <div class="header-navs header-navs-left" id="kt_header_navs">
                                <!--begin::Tab Navs(for tablet and mobile modes)-->
                                <ul class="header-tabs p-5 p-lg-0 d-flex d-lg-none nav nav-bold nav-tabs"
                                    role="tablist">
                                    <!--begin::Item-->
                                    <li class="nav-item mr-2">
                                        <a href="#" class="nav-link btn btn-clean active" data-toggle="tab"
                                            data-target="#kt_header_tab_1" role="tab">Home</a>
                                    </li>
                                    <!--end::Item-->
                                    <!--begin::Item-->
                                    <li class="nav-item mr-2">
                                        <a href="#" class="nav-link btn btn-clean" data-toggle="tab"
                                            data-target="#kt_header_tab_2" role="tab">Reports</a>
                                    </li>
                                    <!--end::Item-->
                                    <!--begin::Item-->
                                    <li class="nav-item mr-2">
                                        <a href="#" class="nav-link btn btn-clean" data-toggle="tab"
                                            data-target="#kt_header_tab_2" role="tab">Orders</a>
                                    </li>
                                    <!--end::Item-->
                                    <!--begin::Item-->
                                    <li class="nav-item mr-2">
                                        <a href="#" class="nav-link btn btn-clean" data-toggle="tab"
                                            data-target="#kt_header_tab_2" role="tab">Help Ceter</a>
                                    </li>
                                    <!--end::Item-->
                                </ul>
                                <!--begin::Tab Navs-->
                                <!--begin::Tab Content-->
                                <div class="tab-content">
                                    <!--begin::Tab Pane-->
                                    <div class="tab-pane py-5 p-lg-0 show active" id="kt_header_tab_1">
                                        <!--begin::Menu-->
                                        <div id="kt_header_menu"
                                            class="header-menu header-menu-mobile header-menu-layout-default">
                                            <!--begin::Nav-->
                                            <ul class="menu-nav">
                                                <li class="menu-item" aria-haspopup="true">
                                                    <a href="/metronic/demo7/index.html" class="menu-link">
                                                        <span class="menu-text">Pasangan</span>
                                                    </a>
                                                </li>
                                                <li class="menu-item" aria-haspopup="true">
                                                    <a href="/metronic/demo7/index.html" class="menu-link">
                                                        <span class="menu-text">Tribina</span>
                                                    </a>
                                                </li>
                                                <li class="menu-item" aria-haspopup="true">
                                                    <a href="/metronic/demo7/index.html" class="menu-link">
                                                        <span class="menu-text">Keluarga Berencana</span>
                                                    </a>
                                                </li>
                                                <li class="menu-item" aria-haspopup="true">
                                                    <a href="/metronic/demo7/index.html" class="menu-link">
                                                        <span class="menu-text">KMS</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <!--end::Nav-->
                                        </div>
                                        <!--end::Menu-->
                                    </div>
                                    <!--begin::Tab Pane-->
                                    <!--begin::Tab Pane-->
                                    <div class="tab-pane p-5 p-lg-0 justify-content-between" id="kt_header_tab_2">
                                        <div
                                            class="d-flex flex-column flex-lg-row align-items-start align-items-lg-center">
                                            <!--begin::Actions-->
                                            <a href="#"
                                                class="btn btn-light-success font-weight-bold mr-3 my-2 my-lg-0">Latest
                                                Orders</a>
                                            <a href="#"
                                                class="btn btn-light-primary font-weight-bold my-2 my-lg-0">Customer
                                                Service</a>
                                            <!--end::Actions-->
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <!--begin::Actions-->
                                            <a href="#" class="btn btn-danger font-weight-bold my-2 my-lg-0">Generate
                                                Reports</a>
                                            <!--end::Actions-->
                                        </div>
                                    </div>
                                    <!--begin::Tab Pane-->
                                </div>
                                <!--end::Tab Content-->
                            </div>
                            <!--end::Header Menu Wrapper-->
                        </div>
                        <!--end::Container-->
                    </div>
                    <!--end::Bottom-->
                </div>
                <!--end::Header-->

                <!--begin::Content-->
                <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                    @yield('content')
                </div>
                <!--end::Content-->

                <div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
                    <div
                        class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
                        <div class="text-dark order-2 order-md-1">
                            <span class="text-muted font-weight-bold mr-2">2020©</span>
                            <a href="{{ route('home') }}" target="_blank"
                                class="text-dark-75 text-hover-primary">Keluarga
                                Berencana</a>
                        </div>
                        <div class="nav nav-dark order-1 order-md-2">
                            <a href="#" target="_blank" class="nav-link pr-3 pl-0">Link 1</a>
                            <a href="#" target="_blank" class="nav-link px-3">Link 2</a>
                            <a href="#" target="_blank" class="nav-link pl-3 pr-0">Link 3</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <x-scroll></x-scroll>

</body>

@endsection