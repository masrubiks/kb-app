<script>
    $('.get_nik').keyup(function(){
        if(this.value.length == 16){
            var _id = $(this).data('id');

            $.ajax({
            url: "{{ route('helper.getnik') }}",
            type:"POST",
            data:{nik : this.value, _token: '{{csrf_token()}}'},
            success:function(res){
                    if(res.status) {
                            $('.for-'+_id).val(res.data.NAMA_LGKP)
                    }else{
                        alert('Data NIK tidak valid!')
                    }
                },
            });
        }
    });

    $('select[name="no_prop"]').on('change', function(){
        $('select[name="no_kab"]').html('<option></option>');
        $('select[name="no_kec"]').html('<option></option>');
        $('select[name="no_kel"]').html('<option></option>');
        $.ajax({
            url: "{{ route('helper.getkab') }}",
            type:"POST",
            data:{provinsi : this.value, _token: '{{csrf_token()}}'},
            success:function(res){
                if(res) {
                    var kabupaten = `<option></option>`;
                    $.each(res, function(i, val){
                       kabupaten += `<option value="`+ val.id +`">`+ val.name +`</option>`;
                    })

                    $('select[name="no_kab"]').html(kabupaten)

                }else{
                    alert('Data provinsi tidak valid!')
                }
            },
        });
    });

    $('select[name="no_kab"]').on('change', function(){
        $('select[name="no_kec"]').html('<option></option>');
        $('select[name="no_kel"]').html('<option></option>');
        $.ajax({
            url: "{{ route('helper.getkec') }}",
            type:"POST",
            data:{kabupaten : this.value, _token: '{{csrf_token()}}'},
            success:function(res){
                if(res) {
                    var kecamatan = `<option></option>`;
                    $.each(res, function(i, val){
                    kecamatan += `<option value="`+ val.id +`">`+ val.name +`</option>`;
                    })
                    
                    $('select[name="no_kec"]').html(kecamatan)
                    
                }else{
                    alert('Data kabupaten tidak valid!')
                }
            },
        });
    });

    $('select[name="no_kec"]').on('change', function(){
        $('select[name="no_kel"]').html('<option></option>');
        var kec = this.value

        $.ajax({
            url: "{{ route('helper.getkel') }}",
            type:"POST",
            data:{kecamatan : this.value, _token: '{{csrf_token()}}'},
            success:function(res){
                if(res) {
                    var kelurahan = `<option></option>`;
                    $.each(res, function(i, val){
                        kelurahan += `<option value="`+ val.id +`">`+ val.name +`</option>`;
                    })

                    $('select[name="no_kel"]').html(kelurahan)

                    no_kel = kec + $('.no_poktan').text()
                    $('.kelompok').val(no_kel);    
                }else{
                    alert('Data kecamatan tidak valid!')
                }
            },
        });
    });

    $('input:radio[name="sk_status"]').on('change', function(){
        if(this.value == 'true'){
            $('.expand').removeClass('d-none')
        }else{
            $('.expand').addClass('d-none')
        }
    })

    $(".add").click(function() {
        var clone = $(".parent:first").clone();
        clone.appendTo(".clone");
        $('.clone .parent:last .number-label').html($(".parent").length)
        $('.clone .parent:last .d-none').removeClass('d-none')
        $('.clone .parent:last .for-1').toggleClass('for-'+$(".parent").length)
        $('.clone .parent:last .for-1').removeClass('for-1')

        $('.clone .parent:last input[name="anggota_nik[]"]').val("")
        $('.clone .parent:last input[name="anggota_nama[]"]').val("")
    });

    function remove(data){
        data.parentNode.parentNode.parentNode.remove();
    }

    $('input[type="checkbox"]').on('change', function(){
        if(this.checked == true){
            $(this).prev().val(this.checked)
        }else{
            $(this).prev().val(this.checked)
        }
    })

    $('.datepicker').datepicker({
        todayHighlight: true,
        orientation: "bottom left",
        autoclose: true,
        format: 'yyyy-mm-dd',
    });

    $(".select2").select2({
        placeholder: "-- Pilih Salah Satu --",
        allowClear: false
    });
</script>