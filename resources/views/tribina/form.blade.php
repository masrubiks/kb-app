@csrf
<!--begin::Wizard Step 1-->
<div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
    <div class="no_poktan d-none">{{ $poktan[0]->value }}</div>
    <input type="hidden" class="form-control form-control-lg" name="kode_kelompok" value="{{ $poktan[0]->label }}">
    <input type="hidden" class="form-control form-control-lg kelompok" name="no_kelompok" value="">
    <div class="card card-custom shadow-sm mb-5">
        <div class="card-header bg-success">
            <div class="card-title">
                <h3 class="card-label text-white">IDENTITAS KELOMPOK</h3>
            </div>
        </div>
        <div class="card-body">
            <div class="form-group fv-plugins-icon-container">
                <label class="font-weight-bold">NAMA KELOMPOK</label>
                <input type="text" class="form-control form-control-lg" name="nama_kelompok"
                    placeholder="Masukkan Nama Kelompok" value="">
                <div class="fv-plugins-message-container"></div>
            </div>
            <hr>
            <!--end::Input-->
            <label class="font-weight-bold pb-5 italic">ALAMAT</label>
            <div class="row">
                <div class="col-xl-6">
                    <!--begin::Select-->
                    <div class="form-group fv-plugins-icon-container">
                        <label>Provinsi</label>
                        <select name="no_prop" class="form-control form-control-lg select2" style="width:100%">
                            <option></option>
                            @foreach ($provinsi as $prov)
                            <option value="{{ $prov->id }}">{{ $prov->name }}
                            </option>
                            @endforeach
                        </select>
                        <div class="fv-plugins-message-container"></div>
                    </div>
                    <!--end::Select-->
                </div>
                <div class="col-xl-6">
                    <!--begin::Select-->
                    <div class="form-group fv-plugins-icon-container">
                        <label>Kabupaten</label>
                        <select name="no_kab" class="form-control form-control-lg select2" style="width:100%">
                            <option></option>
                        </select>
                        <div class="fv-plugins-message-container"></div>
                    </div>
                    <!--end::Select-->
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6">
                    <!--begin::Select-->
                    <div class="form-group fv-plugins-icon-container">
                        <label>Kecamatan</label>
                        <select name="no_kec" class="form-control form-control-lg select2" style="width:100%">
                            <option></option>
                        </select>
                        <div class="fv-plugins-message-container"></div>
                    </div>
                    <!--end::Select-->
                </div>
                <div class="col-xl-6">
                    <!--begin::Select-->
                    <div class="form-group fv-plugins-icon-container">
                        <label>Kelurahan</label>
                        <select name="no_kel" class="form-control form-control-lg select2" style="width:100%">
                            <option></option>
                        </select>
                        <div class="fv-plugins-message-container"></div>
                    </div>
                    <!--end::Select-->
                </div>
            </div>
            <!--begin::Input-->
            <div class="form-group">
                <label>Jalan</label>
                <input type="text" class="form-control form-control-lg" name="alamat" placeholder="Nama Jalan" value="">
            </div>
            <!--end::Input-->
            <div class="row">
                <div class="col-xl-6">
                    <!--begin::Input-->
                    <div class="form-group fv-plugins-icon-container">
                        <label>RT</label>
                        <input type="text" class="form-control form-control-lg" name="no_rt" placeholder="Masukan No RT"
                            value="">
                        {{-- <span class="form-text text-muted">Please enter your Postcode.</span> --}}
                        <div class="fv-plugins-message-container"></div>
                    </div>
                    <!--end::Input-->
                </div>
                <div class="col-xl-6">
                    <!--begin::Input-->
                    <div class="form-group fv-plugins-icon-container">
                        <label>RW</label>
                        <input type="text" class="form-control form-control-lg" name="no_rw" placeholder="Masukan No RW"
                            value="">
                        {{-- <span class="form-text text-muted">Please enter your City.</span> --}}
                        <div class="fv-plugins-message-container"></div>
                    </div>
                    <!--end::Input-->
                </div>
            </div>
            <hr>

            <!--begin::Input-->
            <label class="font-weight-bold pb-5">PEMBINA</label>
            <div class="form-group fv-plugins-icon-container">
                <label>Nama</label>
                <input type="text" class="form-control form-control-lg" name="nama_pembina"
                    placeholder="Masukkan Nama Pembina" value="">
                {{-- <span class="form-text text-muted">Please enter your Address.</span> --}}
                <div class="fv-plugins-message-container"></div>
            </div>
            <!--end::Input-->
            <!--begin::Select-->
            <div class="form-group fv-plugins-icon-container">
                <label>Jabatan Pembina</label>
                <select name="jabatan_pembina" class="form-control form-control-lg select2" style="width:100%">
                    <option></option>
                    @foreach ($jabatans as $jabatan)
                    <option value="{{ $jabatan->value }}">{{ $jabatan->label }}
                    </option>
                    @endforeach
                </select>
                <div class="fv-plugins-message-container"></div>
            </div>
            <!--end::Select-->
        </div>
    </div>
</div>
<!--end::Wizard Step 1-->
<!--begin::Wizard Step 2-->
<div class="pb-5" data-wizard-type="step-content">

    <div class="card card-custom shadow-sm mb-5">
        <div class="card-header bg-success">
            <div class="card-title">
                <h3 class="card-label text-white">INFORMASI KELOMPOK</h3>
            </div>
        </div>
        <div class="card-body">

            {{-- <h4 class="mb-10 font-weight-bold text-dark">INFORMASI KELOMPOK</h4> --}}
            <!--begin::Input-->
            <div class="form-group fv-plugins-icon-container">
                <label class="font-weight-bold pb-3">SK PENGUKUHAN</label>

                <div class="form-group">
                    <div class="radio-inline">
                        <label class="radio">
                            <input type="radio" name="sk_status" value="true">
                            <span></span> Ada</label>
                        <label class="radio">
                            <input type="radio" name="sk_status" value="false" checked>
                            <span></span> Tidak Ada</label>
                    </div>
                </div>
            </div>
            <hr class="pb-5">
            <!--end::Input-->
            <div class="expand d-none">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="font-weight-bold">NO SK</label>
                            <input type="text" class="form-control form-control-lg" name="sk_nomor"
                                placeholder="Masukkan No SK Lengkap" value="">
                            <div class="fv-plugins-message-container"></div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="font-weight-bold">TANGGAL SK</label>
                            <div class="input-group date">
                                <input type="text" name="sk_tanggal" class="form-control datepicker" readonly="readonly"
                                    placeholder="Masukkan Tanggal SK">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="fv-plugins-message-container"></div>
                        </div>
                    </div>
                </div>

                <!--begin::Select-->
                <div class="form-group fv-plugins-icon-container">
                    <label>Dikeluarkan Oleh</label>
                    <select name="sk_pengesah" class="select2 form-control form-control-lg" style="width:100%">
                        <option></option>
                        @foreach ($pengesah as $pengesah_sk)
                        <option value="{{ $pengesah_sk->value }}">
                            {{ $pengesah_sk->label }}
                        </option>
                        @endforeach
                    </select>
                    <div class="fv-plugins-message-container"></div>
                </div>
                <!--end::Select-->

                <hr class="pb-5">
            </div>

            <!--begin::Select-->
            <div class="form-group fv-plugins-icon-container">
                <label class="font-weight-bold">SUMBER DANA KEGIATAN
                    KELOMPOK</label>
                <select name="sumber_dana" class="form-control form-control-lg select2" style="width:100%">
                    <option></option>
                    @foreach ($sumber_dana as $dana)
                    <option value="{{ $dana->value }}">
                        {{ $dana->label }}
                    </option>
                    @endforeach
                </select>
                <div class="fv-plugins-message-container"></div>
            </div>
            <!--end::Select-->

            <!--begin::Select-->
            <div class="form-group fv-plugins-icon-container">
                <label class="font-weight-bold">KETERPADUAN KELOMPOK</label>
                <select name="keterpaduan" class="form-control form-control-lg select2" style="width:100%">
                    <option></option>
                    @foreach ($keterpaduan as $keterpaduan_kel)
                    <option value="{{ $keterpaduan_kel->value }}">
                        {{ $keterpaduan_kel->label }}
                    </option>
                    @endforeach
                </select>
                <div class="fv-plugins-message-container"></div>
            </div>
            <!--end::Select-->
        </div>
    </div>
</div>
<!--end::Wizard Step 2-->
<!--begin::Wizard Step 3-->
<div class="pb-5" data-wizard-type="step-content">
    <div class="card card-custom shadow-sm mb-5">
        <div class="card-header bg-success">
            <div class="card-title">
                <h3 class="card-label text-white">PENGURUS KELOMPOK</h3>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl-2 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <label class="font-weight-bold text-center">JABATAN</label>
                    </div>
                </div>
                <div class="col-xl-4 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <label class="font-weight-bold text-center">NIK</label>
                    </div>
                </div>
                <div class="col-xl-4 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <label class="font-weight-bold text-center">NAMA</label>
                    </div>
                </div>
                <div class="col-xl-2 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <label class="font-weight-bold text-center">PELATIHAN
                            BKB</label>
                    </div>
                </div>
            </div>

            @foreach ($pengurus as $pengelola)
            @if ($pengelola->type == 'pengurus')
            <div class="row">
                <input type="hidden" class="kelompok" name="pengelola_no[]" value="">
                <div class="col-xl-2">
                    <div class="form-group fv-plugins-icon-container mt-3">
                        <label>{{ $pengelola->label }}</label>
                        <input type="hidden" class="form-control form-control-lg" name="pengelola_jabatan[]"
                            value="{{ $pengelola->value }}">
                        <div class="fv-plugins-message-container"></div>

                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="form-group fv-plugins-icon-container">
                        <input type="text" class="form-control form-control-lg get_nik" name="pengelola_nik[]"
                            placeholder="Masukkan NIK" data-id="{{ $pengelola->id }}" value="">
                        <div class="fv-plugins-message-container"></div>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="form-group fv-plugins-icon-container">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-lg for-{{ $pengelola->id }}"
                                name="pengelola_nama[]" readonly placeholder="Masukkan Nama" value="">
                            <div class="fv-plugins-message-container"></div>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-edit"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <span class="switch switch-outline switch-icon switch-success">
                            <label>
                                <input type="hidden" name="pengelola_pelatihan[]" value="false">
                                <input type="checkbox">
                                <span></span>
                            </label>
                        </span>
                    </div>
                </div>
            </div>
            @endif
            @endforeach

        </div>
    </div>
    <div class="card card-custom shadow-sm">
        <div class="card-header bg-success">
            <div class="card-title">
                <h3 class="card-label text-white">KADER KELOMPOK UMUR
                </h3>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl-2 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <label class="font-weight-bold text-center">KELOMPOK
                            UMUR</label>
                    </div>
                </div>
                <div class="col-xl-4 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <label class="font-weight-bold text-center">NIK</label>
                    </div>
                </div>
                <div class="col-xl-4 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <label class="font-weight-bold text-center">NAMA</label>
                    </div>
                </div>
                <div class="col-xl-2 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <label class="font-weight-bold text-center">PELATIHAN
                            BKB</label>
                    </div>
                </div>
            </div>

            @foreach ($pengurus as $pengelola)
            @if ($pengelola->type == 'kader')
            <div class="row">
                <input type="hidden" class="kelompok" name="pengelola_no[]" value="">
                <div class="col-xl-2">
                    <div class="form-group fv-plugins-icon-container mt-3">
                        <label>{{ $pengelola->label }}</label>
                        <input type="hidden" class="form-control form-control-lg" name="pengelola_jabatan[]"
                            value="{{ $pengelola->value }}">
                        <div class="fv-plugins-message-container"></div>

                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="form-group fv-plugins-icon-container">
                        <input type="text" class="form-control form-control-lg get_nik" name="pengelola_nik[]"
                            data-id="{{ $pengelola->id }}" placeholder="Masukkan NIK" value="">
                        <div class="fv-plugins-message-container"></div>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="form-group fv-plugins-icon-container">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-lg for-{{ $pengelola->id }}"
                                name="pengelola_nama[]" readonly placeholder="Masukkan Nama" value="">
                            <div class="fv-plugins-message-container"></div>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-edit"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <span class="switch switch-outline switch-icon switch-success">
                            <label>
                                <input type="hidden" name="pengelola_pelatihan[]" value="false">
                                <input type="checkbox">
                                <span></span>
                            </label>
                        </span>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>
<!--end::Wizard Step 3-->
<!--begin::Wizard Step 4-->
<div class="pb-5" data-wizard-type="step-content">

    <div class="card card-custom shadow-sm">
        <div class="card-header bg-success">
            <div class="card-title">
                <h3 class="card-label text-white">KETERSEDIAAN SARANA
                </h3>
            </div>
        </div>
        <div class="card-body">
            <label class="font-weight-bold font-size-h4 pt-3">BUKU
                MATERI</label>
            <hr>
            @foreach ($sarana as $saranas)
            @if ($saranas->type == 'buku_materi')
            <div class="row">
                <input type="hidden" class="kelompok" name="sarana_no[]" value="">
                <div class="col-xl-9 col-xs-5">
                    <div class="form-group fv-plugins-icon-container">
                        <label>{{ $saranas->label }}</label>
                        <input type="hidden" readonly class="form-control form-control-lg text-white"
                            name="sarana_value[]" value="{{ $saranas->value }}">
                        <div class="fv-plugins-message-container"></div>
                    </div>
                </div>
                <div class="col-xl-3 col-xs-7 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <span class="switch switch-outline switch-icon switch-success">
                            <label>
                                <input type="hidden" name="sarana_ketersediaan[]" value="false">
                                <input type="checkbox">
                                <span></span>
                            </label>
                        </span>
                    </div>
                </div>
            </div>
            @endif
            @endforeach

            <label class="font-weight-bold font-size-h4 pt-3">MATERI KESEHATAN
                REPRODUKSI</label>
            <hr>
            @foreach ($sarana as $saranas)
            @if ($saranas->type == 'materi_kesehatan')
            <div class="row">
                <input type="hidden" class="kelompok" name="sarana_no[]" value="">
                <div class="col-xl-9 col-xs-9">
                    <div class="form-group fv-plugins-icon-container">
                        <label>{{ $saranas->label }}</label>
                        <input type="hidden" readonly class="form-control form-control-lg text-white"
                            name="sarana_value[]" value="{{ $saranas->value }}">
                        <div class="fv-plugins-message-container"></div>
                    </div>
                </div>
                <div class="col-xl-3 col-xs-3 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <span class="switch switch-outline switch-icon switch-success">
                            <label>
                                <input type="hidden" name="sarana_ketersediaan[]" value="false">
                                <input type="checkbox">
                                <span></span>
                            </label>
                        </span>
                    </div>
                </div>
            </div>
            @endif
            @endforeach

            <label class="font-weight-bold font-size-h4 pt-3">SARANA
                LAINNYA</label>
            <hr>
            @foreach ($sarana as $saranas)
            @if ($saranas->type == 'sarana_lainnya')
            <div class="row">
                <input type="hidden" class="kelompok" name="sarana_no[]" value="">
                <div class="col-xl-9 col-xs-9">
                    <div class="form-group fv-plugins-icon-container">
                        <label>{{ $saranas->label }}</label>
                        <input type="hidden" readonly class="form-control form-control-lg text-white"
                            name="sarana_value[]" value="{{ $saranas->value }}">
                        <div class="fv-plugins-message-container"></div>
                    </div>
                </div>
                <div class="col-xl-3 col-xs-3 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <span class="switch switch-outline switch-icon switch-success">
                            <label>
                                <input type="hidden" name="sarana_ketersediaan[]" value="false">
                                <input type="checkbox">
                                <span></span>
                            </label>
                        </span>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>
<!--end::Wizard Step 4-->
<!--begin::Wizard Step 5-->
<div class="pb-5" data-wizard-type="step-content">
    <!--begin::Section-->

    <div class="card card-custom shadow-sm">
        <div class="card-header bg-success">
            <div class="card-title">
                <h3 class="card-label text-white">INFORMASI ANGGOTA
                </h3>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl-5 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <label class="font-weight-bold text-center">NIK</label>
                    </div>
                </div>
                <div class="col-xl-6 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <label class="font-weight-bold text-center">NAMA</label>
                    </div>
                </div>
                <div class="col-xl-1 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container">
                        <a class="add btn btn-success btn-sm text-center">
                            <i class="flaticon2-plus px-0"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row parent">
                <input type="hidden" class="kelompok" name="anggota_no[]" value="">
                <div class="col-xl-5">
                    <div class="form-group fv-plugins-icon-container">
                        <input type="text" class="form-control form-control-lg get_nik" data-id="1" name="anggota_nik[]"
                            placeholder="Masukkan NIK" value="">
                        <div class="fv-plugins-message-container"></div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="form-group fv-plugins-icon-container">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-lg for-1" name="anggota_nama[]" readonly
                                placeholder="Masukkan Nama" value="">
                            <div class="fv-plugins-message-container"></div>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-edit"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-1 d-flex justify-content-center">
                    <div class="form-group fv-plugins-icon-container d-none">
                        <div class="btn btn-danger btn-sm text-center" onclick="remove(this)">
                            <i class="flaticon2-delete px-0"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clone"></div>

        </div>
    </div>
</div>
<!--end::Wizard Step 5-->
<!--begin::Wizard Actions-->
<div class="d-flex justify-content-between">
    <div class="mr-2">
        <button type="button" class="btn btn-light-primary font-weight-bolder text-uppercase px-9 py-4"
            data-wizard-type="action-prev">Sebelumnya</button>
    </div>
    <div>
        <button type="button" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4"
            data-wizard-type="action-submit">Simpan</button>
        <button type="button" class="btn btn-primary font-weight-bolder text-uppercase px-9 py-4"
            data-wizard-type="action-next">Selanjutnya</button>
    </div>
</div>
<!--end::Wizard Actions-->
<div></div>
<div></div>
<div></div>
<div></div>