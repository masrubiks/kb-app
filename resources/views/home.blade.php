@extends('layouts.frontend')

@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <div class="d-lg-flex flex-row-fluid">
            <div class="content-wrapper flex-row-fluid">
                <!--begin::Card-->
                <div class="card card-custom card-stretch example example-compact" id="kt_page_stretched_card">
                    <div class="card-header text-center" kt-hidden-height="74" style="">
                        <div class="card-title">
                            <h3 class="card-label">
                                Home Page
                                <small>Statistik Data Keluarga Berencana (KB)</small>
                            </h3>
                        </div>
                    </div>
                    <div class="card-body">
                    </div>
                </div>
                <!--end::Card-->
            </div>
        </div>
    </div>
    <!--end::Container-->
</div>
<!--end::Entry-->
@endsection