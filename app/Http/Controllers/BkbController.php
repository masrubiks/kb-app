<?php

namespace App\Http\Controllers;

use App\Http\Controllers\HelperController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BkbController extends Controller
{
    public function __construct()
    {
        $this->helper = new HelperController;
    }


    public function index()
    {
        $list = $this->helper->get_table('https://api.jogjakota.go.id/kelompok');

        return view(
            'tribina.bkb.table',
            array('list' => $list->data)
        );
    }

    public function create()
    {
        return view(
            'tribina.bkb.create',
            array(
                'provinsi' => $this->helper->get_prov(),
                'jabatans' => $this->helper->get_master(array('category' => 'jabatan')),
                'pengesah' => $this->helper->get_master(array('category' => 'pengesah')),
                'sumber_dana' => $this->helper->get_master(array('category' => 'sumber_dana')),
                'keterpaduan' => $this->helper->get_master(array('category' => 'keterpaduan_bkb')),
                'pengurus' => $this->helper->get_master(array('category' => 'pengurus_bkb')),
                'sarana' => $this->helper->get_master(array('category' => 'sarana_bkb')),
                'poktan' => $this->helper->get_master(array('category' => 'kode_poktan')),
            )
        );
    }

    public function store(Request $request)
    {
        $response = Http::withOptions(['verify' => false])->post('http://127.0.0.1:3000/kelompok', $request->all());

        return json_decode($response, true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
