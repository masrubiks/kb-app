<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HelperController extends Controller
{
    public function get_table($url, $param = array('page' => 1, 'limit' => 10))
    {
        $response = Http::withOptions(['verify' => false])
            ->get($url, $param);

        return json_decode($response->body());
    }

    public function get_prov()
    {
        $response = Http::withOptions(['verify' => false])
            ->get('https://jss.jogjakota.go.id/siwarga/api/master/prov');

        return json_decode($response->body());
    }

    public function get_kab(Request $request)
    {
        $response = Http::withOptions(['verify' => false])
            ->get('https://jss.jogjakota.go.id/siwarga/api/master/kab?provId=' . $request->provinsi);

        return json_decode($response->body());
    }

    public function get_kec(Request $request)
    {
        $response = Http::withOptions(['verify' => false])
            ->get('https://jss.jogjakota.go.id/siwarga/api/master/kec?kabId=' . $request->kabupaten);

        return json_decode($response->body());
    }

    public function get_kel(Request $request)
    {
        $response = Http::withOptions(['verify' => false])
            ->get('https://jss.jogjakota.go.id/siwarga/api/master/kel?kecId=' . $request->kecamatan);

        return json_decode($response->body());
    }

    public function get_nik(Request $request)
    {
        $response = Http::withOptions(['verify' => false])
            ->get('https://webservice.jogjakota.go.id/ws_nik/index.php/kependudukan/nasional?nik=' . $request->nik . '&reverse=true');

        return json_decode($response, true);
    }

    public function get_master($req)
    {
        $response = Http::withOptions(['verify' => false])
            ->get('https://api.jogjakota.go.id/masters', $req);

        $result = json_decode($response->body());

        return $result->data;
    }
}
