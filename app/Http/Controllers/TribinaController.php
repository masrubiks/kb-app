<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class TribinaController extends Controller
{
    public function bkb()
    {
        $response_jabatan = Http::withOptions(['verify' => false])
            ->get('https://api.jogjakota.go.id/masters', [
                'category' => 'jabatan'
            ]);

        $response_pengesah = Http::withOptions(['verify' => false])
            ->get('https://api.jogjakota.go.id/masters', [
                'category' => 'pengesah'
            ]);

        $response_sumber_dana = Http::withOptions(['verify' => false])
            ->get('https://api.jogjakota.go.id/masters', [
                'category' => 'sumber_dana'
            ]);

        $response_keterpaduan = Http::withOptions(['verify' => false])
            ->get('https://api.jogjakota.go.id/masters', [
                'category' => 'keterpaduan_bkb'
            ]);

        $response_pengurus = Http::withOptions(['verify' => false])
            ->get('https://api.jogjakota.go.id/masters', [
                'category' => 'pengurus_bkb'
            ]);

        $response_sarana = Http::withOptions(['verify' => false])
            ->get('https://api.jogjakota.go.id/masters', [
                'category' => 'sarana_bkb'
            ]);

        $response_provinsi = Http::withOptions(['verify' => false])
            ->get('https://jss.jogjakota.go.id/siwarga/api/master/prov');

        $jabatan = json_decode($response_jabatan->body());
        $pengesah = json_decode($response_pengesah->body());
        $sumber_dana = json_decode($response_sumber_dana->body());
        $keterpaduan = json_decode($response_keterpaduan->body());
        $pengurus = json_decode($response_pengurus->body());
        $sarana = json_decode($response_sarana->body());
        $provinsi = json_decode($response_provinsi->body());

        return view(
            'tribina/bkb',
            array(
                'jabatans' => $jabatan->data,
                'pengesah' => $pengesah->data,
                'sumber_dana' => $sumber_dana->data,
                'keterpaduan' => $keterpaduan->data,
                'pengurus' => $pengurus->data,
                'sarana' => $sarana->data,
                'provinsi' => $provinsi,
            )
        );
    }

    public function getnik(Request $request)
    {
        $response = Http::withOptions(['verify' => false])
            ->get('https://webservice.jogjakota.go.id/ws_nik/index.php/kependudukan/nasional?nik=' . $request->nik . '&reverse=true');

        return json_decode($response, true);
    }

    public function getkab(Request $request)
    {
        $response = Http::withOptions(['verify' => false])
            ->get('https://jss.jogjakota.go.id/siwarga/api/master/kab?provId=' . $request->provinsi);

        return json_decode($response, true);
    }

    public function getkec(Request $request)
    {
        $response = Http::withOptions(['verify' => false])
            ->get('https://jss.jogjakota.go.id/siwarga/api/master/kec?kabId=' . $request->kabupaten);

        return json_decode($response, true);
    }

    public function getkel(Request $request)
    {
        $response = Http::withOptions(['verify' => false])
            ->get('https://jss.jogjakota.go.id/siwarga/api/master/kel?kecId=' . $request->kecamatan);

        return json_decode($response, true);
    }
}
