<?php

namespace App\Http\Controllers;

use App\Http\Controllers\HelperController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BklController extends Controller
{
    public function __construct()
    {
        $this->helper = new HelperController;
    }


    public function index()
    {
        //
    }

    public function create()
    {
        return view(
            'tribina.bkl.create',
            array(
                'provinsi' => $this->helper->get_prov(),
                'jabatans' => $this->helper->get_master('jabatan'),
                'pengesah' => $this->helper->get_master('pengesah'),
                'sumber_dana' => $this->helper->get_master('sumber_dana'),
                'keterpaduan' => $this->helper->get_master('keterpaduan_bkb'),
                'pengurus' => $this->helper->get_master('pengurus_bkb'),
                'sarana' => $this->helper->get_master('sarana_bkb'),
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
